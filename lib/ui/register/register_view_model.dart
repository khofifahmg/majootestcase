import 'dart:async';

import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_snackbar.dart';
import 'package:majootestcase/providers/user_provider.dart';
import 'package:majootestcase/ui/beranda/beranda.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/routes.dart';
import 'package:provider/provider.dart';
import './register.dart';

abstract class RegisterViewModel extends State<Register> {
  final formKey = GlobalKey<FormState>();
  String username, email, password;
  UserProvider userProvider = UserProvider();

  onRegister(BuildContext buildContext) async {
    if (formKey.currentState.validate()) {
      userProvider.insertUser(username, email, password).then((value) {
        Scaffold.of(buildContext).showSnackBar(
          customSnackbar(
            buildContext,
            'Register Berhasil',
          ),
        );
        Timer(Duration(milliseconds: 1500), () {
          goTo(context, Beranda());
        });
      });
    } else {
      if (email != null && !validateEmail(email)) {
        Scaffold.of(buildContext).showSnackBar(
          customSnackbar(buildContext, 'Masukkan email yang valid'),
        );
      } else {
        Scaffold.of(buildContext).showSnackBar(
          customSnackbar(
            buildContext,
            'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan',
          ),
        );
      }
    }
  }

  @override
  void initState() {
    userProvider = Provider.of<UserProvider>(context, listen: false);
    super.initState();
  }
}
