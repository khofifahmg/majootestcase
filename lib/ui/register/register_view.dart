import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_field.dart';
import 'package:majootestcase/common/widget/custom_text.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/routes.dart';
import './register_view_model.dart';

class RegisterView extends RegisterViewModel {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: SizedBox(
        width: size.width,
        height: size.height,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Builder(
            builder: (builderContext) => SingleChildScrollView(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(20),
                child: SizedBox(
                  width: size.width,
                  height: size.height,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      CustomText(
                        text: 'Register',
                        fontSize: 24,
                        isBold: true,
                      ),
                      SizedBox(height: 30),
                      Form(
                        key: formKey,
                        child: Column(
                          children: [
                            CustomField(
                              label: 'Username',
                              hint: 'username',
                              isClearButton: true,
                              onChanged: (value) {
                                if (!mounted) return;
                                setState(() {
                                  username = value;
                                });
                              },
                              validator: (value) {
                                if (value == '') {
                                  return 'Username tidak boleh kosong';
                                }
                              },
                            ),
                            SizedBox(height: 20),
                            CustomField(
                              label: 'Email',
                              isEmail: true,
                              isClearButton: true,
                              hint: 'example@123.com',
                              onChanged: (value) {
                                if (!mounted) return;
                                setState(() {
                                  email = value;
                                });
                              },
                              validator: (value) {
                                if (value == '') {
                                  return 'Email tidak boleh kosong';
                                } else if (!validateEmail(email)) {
                                  return 'Masukkan email yang valid';
                                }
                              },
                            ),
                            SizedBox(height: 20),
                            CustomField(
                              label: 'Password',
                              isPassword: true,
                              hint: 'password',
                              onChanged: (value) {
                                if (!mounted) return;
                                setState(() {
                                  password = value;
                                });
                              },
                              validator: (value) {
                                if (value == '') {
                                  return 'Password tidak boleh kosong';
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      CustomButton(
                        label: 'REGISTER',
                        onTap: () => onRegister(builderContext),
                      ),
                      SizedBox(height: 40),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomText(
                            text: 'Sudah punya akun? ',
                          ),
                          InkWell(
                            onTap: () {
                              goBack(context);
                            },
                            child: CustomText(
                              text: 'Login',
                              isBold: true,
                              color: primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
