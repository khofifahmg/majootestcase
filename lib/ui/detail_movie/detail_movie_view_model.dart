import 'dart:async';

import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/providers/connection_provider.dart';
import 'package:majootestcase/providers/movie_provider.dart';
import 'package:majootestcase/services/movie_service.dart';
import 'package:provider/provider.dart';
import './detail_movie.dart';

abstract class DetailMovieViewModel extends State<DetailMovie> {
  MovieProvider movieProvider = MovieProvider();
  ConnectionProvider conProvider = ConnectionProvider();
  List<MovieData> listRecom = [];
  MovieData selectedMovie;
  bool isLoading = true;

  getDetailMovie() async {
    if (!mounted) return;
    setState(() {
      isLoading = true;
    });

    await conProvider.checkConnectivity().then((value) async {
      if (conProvider.isConnect ?? false) {
        await MovieService()
            .getDetail(context, widget.type, widget.id)
            .then((detail) async {
          if (detail == null) {
            if (!mounted) return;
            return setState(() {
              isLoading = false;
            });
          }
          await MovieService()
              .getRecommendation(context, widget.type, widget.id)
              .then((listrecom) {
            if (listrecom == null) {
              if (!mounted) return;
              return setState(() {
                isLoading = false;
              });
            }
            if (!mounted) return;
            setState(() {
              selectedMovie = MovieData.fromJson(detail);
              listRecom = MovieList.fromJson(listrecom).list;
              isLoading = false;
            });
          });
        });
      } else {
        Timer(Duration(seconds: 2), () {
          if (!mounted) return;
          setState(() {
            isLoading = false;
          });
        });
      }
    });
  }

  @override
  void initState() {
    getDetailMovie();
    movieProvider = Provider.of<MovieProvider>(context, listen: false);
    conProvider = Provider.of<ConnectionProvider>(context, listen: false);
    super.initState();
  }
}
