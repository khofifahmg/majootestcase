import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_text.dart';
import 'package:majootestcase/common/widget/show_loading.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/common/widget/top_item.dart';
import 'package:majootestcase/providers/connection_provider.dart';
import 'package:majootestcase/ui/detail_movie/widgets.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/routes.dart';
import 'package:provider/provider.dart';
import './detail_movie_view_model.dart';

class DetailMovieView extends DetailMovieViewModel {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    bool isConnect =
        Provider.of<ConnectionProvider>(context).isConnect ?? false;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          margin: EdgeInsets.only(top: widget.isBack ? 0 : 22),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          width: size.width,
          height: size.height,
          child: isLoading
              ? circleLoading(context)
              : !isConnect
                  ? errorScreen(
                      context,
                      retry: () => getDetailMovie(),
                    )
                  : Stack(
                      children: [
                        bodyDetail(),
                        Positioned(
                          top: 10,
                          left: 10,
                          child: InkWell(
                            onTap: () {
                              goBack(context);
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: primaryColor,
                                shape: BoxShape.circle,
                              ),
                              child: Icon(
                                widget.isBack ? Icons.arrow_back : Icons.close,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
        ),
      ),
    );
  }

  Widget bodyDetail() {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          topItem(context, selectedMovie, true),
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                cardDetail(
                  'Year',
                  selectedMovie.year,
                ),
                SizedBox(height: 20),
                cardDetail(
                  'Popularity',
                  selectedMovie.popularity,
                ),
                SizedBox(height: 20),
                Container(
                  constraints: BoxConstraints(
                    minHeight: size.width * 0.2,
                  ),
                  width: size.width,
                  child: CustomText(
                    text: selectedMovie.overview,
                    useMaxline: false,
                    isOverflow: false,
                    fontSize: 16,
                    align: TextAlign.justify,
                  ),
                ),
                SizedBox(height: 30),
                CustomText(
                  text: 'Reccommendation',
                  fontSize: 18,
                  isBold: true,
                ),
                SizedBox(height: 20),
                listRecommendation(
                  context,
                  listRecom,
                  widget.type,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
