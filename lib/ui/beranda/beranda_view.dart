import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/show_loading.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/common/widget/top_item.dart';
import 'package:majootestcase/providers/connection_provider.dart';
import 'package:majootestcase/providers/movie_provider.dart';
import 'package:majootestcase/ui/beranda/widgets.dart';
import 'package:majootestcase/ui/extra/splash_screen.dart';
import 'package:majootestcase/utils/routes.dart';
import 'package:majootestcase/utils/shared_preferences.dart';
import 'package:provider/provider.dart';
import './beranda_view_model.dart';

class BerandaView extends BerandaViewModel with SharedPreferencesHandler {
  @override
  Widget build(BuildContext context) {
    MovieProvider movie = Provider.of<MovieProvider>(context);
    bool isConnect =
        Provider.of<ConnectionProvider>(context).isConnect ?? false;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: isLoading
            ? circleLoading(context)
            : !isConnect
                ? errorScreen(
                    context,
                    retry: () => getListMovie(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              isDismissible: true,
                              isScrollControlled: true,
                              builder: (_) {
                                return DetailMovie(
                                  type: 'movie',
                                  id: movie.listMovie.list[0].id,
                                );
                              },
                            );
                          },
                          child: topItem(
                            context,
                            movie.listMovie.list[0],
                            false,
                          ),
                        ),
                        listMovieCard(
                          context,
                          'Trending Today',
                          movie.listTrendingToday.list,
                          'trending',
                        ),
                        listMovieCard(
                          context,
                          'Trending This Week',
                          movie.listTrendingWeek.list,
                          'trending',
                        ),
                        listMovieCard(
                          context,
                          'Popular Movies',
                          movie.listMovie.list,
                          'movie',
                        ),
                        listMovieCard(
                          context,
                          'Popular TV Series',
                          movie.listTV.list,
                          'tv',
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: CustomButton(
                            label: 'LOGOUT',
                            backgroundColor: Colors.red[900],
                            borderColor: Colors.red[900],
                            onTap: () {
                              setUserId(null);
                              goToAndRemove(context, SplashScreen());
                            },
                          ),
                        ),
                        SizedBox(height: 30),
                      ],
                    ),
                  ),
      ),
    );
  }
}
