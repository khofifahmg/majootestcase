import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_snackbar.dart';
import 'package:majootestcase/providers/user_provider.dart';
import 'package:majootestcase/ui/beranda/beranda.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/routes.dart';
import 'package:provider/provider.dart';
import './login.dart';

abstract class LoginViewModel extends State<Login> {
  final formKey = GlobalKey<FormState>();
  String email, password;
  UserProvider userProvider = UserProvider();

  onLogin(BuildContext buildContext) async {
    if (formKey.currentState.validate()) {
      userProvider.getDataUser(email, password).then((value) {
        if (value == null) {
          Scaffold.of(buildContext).showSnackBar(
            customSnackbar(
              context,
              'Login gagal, periksa kembali inputan anda',
            ),
          );
        } else {
          goTo(context, Beranda());
        }
      });
    } else {
      if (email != null && !validateEmail(email)) {
        Scaffold.of(buildContext).showSnackBar(
          customSnackbar(buildContext, 'Masukkan email yang valid'),
        );
      } else {
        Scaffold.of(buildContext).showSnackBar(
          customSnackbar(
            buildContext,
            'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan',
          ),
        );
      }
    }
  }

  openDB() async {
    await userProvider.openDB();
    await userProvider.createUserTable();
  }

  @override
  void initState() {
    userProvider = Provider.of<UserProvider>(context, listen: false);
    openDB();
    super.initState();
  }
}
