import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_field.dart';
import 'package:majootestcase/common/widget/custom_text.dart';
import 'package:majootestcase/ui/register/register.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/routes.dart';
import './login_view_model.dart';

class LoginView extends LoginViewModel {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: SizedBox(
        width: size.width,
        height: size.height,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Builder(
            builder: (builderContext) => SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: SizedBox(
                width: size.width,
                height: size.height,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    CustomText(
                      text: 'Selamat Datang',
                      fontSize: 24,
                      isBold: true,
                    ),
                    SizedBox(height: 10),
                    CustomText(
                      text: 'Silahkan login terlebih dahulu',
                      fontSize: 15,
                    ),
                    SizedBox(height: 30),
                    Form(
                      key: formKey,
                      child: Column(
                        children: [
                          CustomField(
                            label: 'Email',
                            hint: 'example@123.com',
                            isEmail: true,
                            isClearButton: true,
                            onChanged: (value) {
                              if (!mounted) return;
                              setState(() {
                                email = value;
                              });
                            },
                            validator: (value) {
                              if (value == '') {
                                return 'Email tidak boleh kosong';
                              } else if (!validateEmail(email)) {
                                return 'Masukkan email yang valid';
                              }
                            },
                          ),
                          SizedBox(height: 20),
                          CustomField(
                            label: 'Password',
                            hint: 'password',
                            isPassword: true,
                            onChanged: (value) {
                              if (!mounted) return;
                              setState(() {
                                password = value;
                              });
                            },
                            validator: (value) {
                              if (value == '') {
                                return 'Password tidak boleh kosong';
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    CustomButton(
                      label: 'LOGIN',
                      onTap: () => onLogin(builderContext),
                    ),
                    SizedBox(height: 40),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          text: 'Belum punya akun? ',
                        ),
                        InkWell(
                          onTap: () {
                            goTo(context, Register());
                          },
                          child: CustomText(
                            text: 'Daftar',
                            isBold: true,
                            color: primaryColor,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
