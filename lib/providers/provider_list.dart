import 'package:majootestcase/providers/connection_provider.dart';
import 'package:majootestcase/providers/movie_provider.dart';
import 'package:majootestcase/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

// List provider yang akan didaftarkan pada method multiprovider di main.dart
List<SingleChildWidget> providerList = [
  ChangeNotifierProvider(create: (_) => UserProvider()),
  ChangeNotifierProvider(create: (_) => MovieProvider()),
  ChangeNotifierProvider(create: (_) => ConnectionProvider()),
];
