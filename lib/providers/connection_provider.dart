import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';

// Class untuk mengecek konseksi
// Author By Khofifah
// Hasil berupa function cek koneksi dan status koneksi
class ConnectionProvider extends ChangeNotifier {
  Connectivity connectivity = Connectivity();
  bool isConnect;

  // Mengubah status koneksi sesuai ConnectivityResult
  Future<void> updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        isConnect = true;
        break;
      case ConnectivityResult.mobile:
        isConnect = true;
        break;
      case ConnectivityResult.none:
        isConnect = false;
        break;
      default:
        isConnect = false;
        break;
    }
    notifyListeners();

    return isConnect;
  }

  // Cek koneksi pada device user
  Future checkConnectivity() async {
    ConnectivityResult result = await connectivity.checkConnectivity();

    await updateConnectionStatus(result);
  }

  // Mengubah status koneksi sesuai value boolean
  setConnectionStatus(bool status) {
    isConnect = status;
    notifyListeners();
  }
}
