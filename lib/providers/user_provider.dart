import 'dart:core';

import 'package:flutter/material.dart';
import 'package:majootestcase/utils/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'package:sqflite/utils/utils.dart';

// Class mengatur data user yang disimpan pada database
// Author By Khofifah
// Hasil berupa tabel user dan data user
class UserProvider extends ChangeNotifier {
  Database db;
  Map dataUser;

  // open database agar bisa digunakan
  openDB() async {
    db = await openDatabase('movie.db');
    notifyListeners();
  }

  // close database agar tidak melakukan call terus-menerus
  closeDB() async {
    await db.close();
    notifyListeners();
  }

  // Membuat table user
  createUserTable() async {
    if (!db.isOpen) openDB();

    // Pengecekan apakah table user sudah ada atau tidak
    tableExists('user_table').then((value) async {
      if (!value)
        await db
            .execute(
          'CREATE TABLE user_table (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, email TEXT, password TEXT)',
        )
            .then((value) {
          print('value');
        });
    });

    notifyListeners();
  }

  // Pengecekan apakah table sudah ada atau tidak
  Future<bool> tableExists(
    String table, // nama table yang ingin dicek
  ) async {
    var count = firstIntValue(
      await db.query('sqlite_master',
          columns: ['COUNT(*)'],
          where: 'type = ? AND name = ?',
          whereArgs: ['table', table]),
    );
    return count > 0;
  }

  // Mendapatkn data user yang tersimpan
  Future getDataUser(
    String email, // Email user
    String password, // Password user
  ) async {
    List<Map> list = await db.query(
      'user_table',
      columns: ['email'],
      where: 'email = ?',
      whereArgs: [email],
    );

    list.where((e) => e['password'] == password).toList();

    if (list.isNotEmpty) {
      dataUser = list[0];
      await SharedPreferencesHandler().setUserId(dataUser['id']);
    }

    notifyListeners();

    return dataUser;
  }

  // Membuat user baru
  insertUser(
    String name, // Username
    String email, // Email user
    String password, // Password user
  ) async {
    int recordId = await db.insert('user_table', {
      'username': name,
      'email': email,
      'password': password,
    });
    await SharedPreferencesHandler().setUserId(recordId);

    notifyListeners();

    return recordId;
  }
}
