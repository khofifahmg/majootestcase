import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

// Class untuk mengubah data film
// Author By Khofifah
// Hasil berupa function untuk mengubah film dan variabel film
class MovieProvider extends ChangeNotifier {
  MovieList listMovie = MovieList(); // List movie berdasarkan popular
  MovieList listTV = MovieList(); // List serial tv berdasarkan popular
  MovieList listTrendingToday =
      MovieList(); // List movie dan serial tv hari ini
  MovieList listTrendingWeek =
      MovieList(); // List movie dan serial tv minggu ini

  // Mengatur value list movie berdasarkan popular
  setListMovie(List data) async {
    listMovie = MovieList.fromJson(data);
    notifyListeners();
  }

  // Mengatur value list serial tv berdasarkan popular
  setListTV(List data) async {
    listTV = MovieList.fromJson(data);
    notifyListeners();
  }

  // Mengatur value list movie dan serial tv hari ini
  setListTrendingToday(List data) async {
    listTrendingToday = MovieList.fromJson(data);
    notifyListeners();
  }

  // Mengatur value list movie dan serial tv minggu ini
  setListTrendingWeek(List data) async {
    listTrendingWeek = MovieList.fromJson(data);
    notifyListeners();
  }
}
