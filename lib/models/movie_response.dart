class MovieList {
  MovieList({
    this.list,
  });
  List<MovieData> list;

  factory MovieList.fromJson(List json) => MovieList(
        list: List<MovieData>.from(
          json.map((x) => MovieData.fromJson(x)),
        ),
      );
}

class MovieData {
  MovieData({
    this.id,
    this.overview,
    this.popularity,
    this.tittle,
    this.vote,
    this.year,
    this.poster,
    this.backdrop,
    this.type,
    this.adult,
  });
  int id;
  String tittle;
  String overview;
  String vote;
  String year;
  String poster;
  String backdrop;
  String type;
  String popularity;
  String adult;

  factory MovieData.fromJson(Map<String, dynamic> json) => MovieData(
        id: json['id'],
        overview: json['overview'],
        popularity: json['popularity'].toString(),
        tittle:
            json['title'] ?? json['original_title'] ?? json['original_name'],
        vote: json['vote_average'].toString(),
        year: json['release_date'].toString().substring(0, 4),
        poster: json['poster_path'],
        backdrop: json['backdrop_path'],
        type: json['media_type'],
        adult: json['adult'] == null
            ? 'All Age'
            : json['adult']
                ? 'Adult'
                : 'Teen',
      );
}
