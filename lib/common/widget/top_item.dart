import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_divider.dart';
import 'package:majootestcase/common/widget/custom_text.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/constant.dart';

Widget topItem(
  BuildContext context,
  MovieData data,
  bool isDetail,
) {
  Size size = MediaQuery.of(context).size;
  return SizedBox(
    width: size.width,
    child: Column(
      children: [
        Stack(
          children: [
            Container(
              width: size.width,
              height: isDetail ? null : size.width,
              padding: EdgeInsets.only(bottom: 25),
              constraints: BoxConstraints(minHeight: size.width * 0.1),
              child: Image.network(
                urlImage + (isDetail ? data.backdrop : data.poster),
                fit: isDetail ? BoxFit.fitWidth : BoxFit.cover,
              ),
            ),
            Positioned(
              bottom: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: size.width,
                    padding: EdgeInsets.fromLTRB(10, 70, 10, 10),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: <Color>[
                          Colors.transparent,
                          primaryColor,
                        ],
                        tileMode: TileMode.clamp,
                      ),
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          CustomText(
                            text: data.vote,
                            align: TextAlign.center,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                          customVerDivider(),
                          CustomText(
                            text: data.adult,
                            align: TextAlign.center,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(10),
          width: size.width,
          decoration: BoxDecoration(
            color: primaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
          ),
          child: CustomText(
            text: data.tittle,
            align: TextAlign.center,
            color: Colors.white,
            fontSize: 18,
            useMaxline: false,
            isBold: true,
          ),
        ),
      ],
    ),
  );
}
