import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_text.dart';
import 'package:majootestcase/utils/constant.dart';

customSnackbar(
  BuildContext context,
  String text, {
  Color backColor,
  Color textColor,
}) {
  return SnackBar(
    backgroundColor: backColor ?? thirdColor,
    content: CustomText(
      text: text,
      color: textColor ?? Colors.white,
      isBold: true,
      fontSize: 14,
      useMaxline: false,
      isOverflow: false,
    ),
    duration: const Duration(seconds: 2),
  );
}
