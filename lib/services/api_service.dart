import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/providers/connection_provider.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:provider/provider.dart';

// Class untuk inisasi fungsi dio
// Author By Khofifah
// Hasil berupa function untuk mengakses api
class ApiServices {
  // Inisiasi dio dengan options
  Dio dio = new Dio(
    BaseOptions(
      baseUrl: serverUrl,
      receiveDataWhenStatusError: true,
      connectTimeout: 4000,
      receiveTimeout: 4000,
    ),
  );

  // Function penggunaan dio untuk method get
  // Hasil berupa response api
  Future<Response> getApi(
    BuildContext context,
    String url, // url api
  ) async {
    Response response;
    ConnectionProvider conProvider =
        Provider.of<ConnectionProvider>(context, listen: false);

    if (conProvider.isConnect ?? false) {
      try {
        response = await dio.get(url);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.CONNECT_TIMEOUT) {
          conProvider.setConnectionStatus(false);
        }
        conProvider.setConnectionStatus(false);
      }
    }

    return response;
  }

  // Function penggunaan dio untuk method post
  // Hasil berupa response api
  Future<Response> postApi(
    BuildContext context,
    String url, // url api
    Map data, // request data
  ) async {
    Response response;
    ConnectionProvider conProvider =
        Provider.of<ConnectionProvider>(context, listen: false);

    if (conProvider.isConnect ?? false) {
      try {
        response = await dio.post(
          url,
          data: jsonEncode(data),
        );
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.CONNECT_TIMEOUT) {
          conProvider.setConnectionStatus(false);
        }
        conProvider.setConnectionStatus(false);
      }
    }

    return response;
  }
}
