import 'package:flutter/material.dart';
import 'package:majootestcase/providers/movie_provider.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:provider/provider.dart';

// Class untuk mengakses api movie
// Author By Khofifah
// Hasil berupa list dan disimpan di provider
class MovieService {
  // Mendapatkan list sesuai trending movie dan serial tv berdasarkan periode
  // Hasil berupa list dan disimpan di provider
  getListTrending(
    BuildContext context,
    String date, // periode waktu yang ditetapkan (day atau week)
  ) async {
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/trending/all/$date?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        if (date == 'day') {
          Provider.of<MovieProvider>(context, listen: false)
              .setListTrendingToday(
            value.data['results'],
          );
        } else {
          Provider.of<MovieProvider>(context, listen: false)
              .setListTrendingWeek(
            value.data['results'],
          );
        }
      }
    });
  }

  // Mendapatkan list movie sesuai populer
  // Hasil berupa list dan disimpan di provider
  getListMovie(BuildContext context) async {
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/movie/popular?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        Provider.of<MovieProvider>(context, listen: false).setListMovie(
          value.data['results'],
        );
      }
    });
  }

  // Mendapatkan list serial tv sesuai populer
  // Hasil berupa list dan disimpan di provider
  getListTV(BuildContext context) async {
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/tv/popular?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        Provider.of<MovieProvider>(context, listen: false).setListTV(
          value.data['results'],
        );
      }
    });
  }

  // Mendapatkan detail film (movie dan serial tv) berdasarkan id
  // Hasil berupa object dan disimpan di provider
  Future getDetail(
    BuildContext context,
    String type, // jenis film (movie atau tv)
    int id, // id film
  ) async {
    Map response;
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/$type/$id?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        response = value.data;
      }
    });
    return response;
  }

  // Mendapatkan rekomendasi film (movie dan serial tv) berdasarkan id
  // Hasil berupa list dan disimpan di provider
  Future getRecommendation(
    BuildContext context,
    String type, // jenis film (movie atau tv)
    int id, // id film
  ) async {
    List response = [];
    await ApiServices()
        .getApi(
      context,
      '$serverUrl/$type/$id/recommendations?api_key=$apiKey',
    )
        .then((value) {
      if (value != null) {
        response = value.data['results'];
      }
    });
    return response;
  }
}
