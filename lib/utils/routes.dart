import 'package:flutter/material.dart';

goTo(BuildContext context, dynamic page) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => page),
  );
}

goBack(BuildContext context) {
  Navigator.pop(context);
}

goToAndRemove(BuildContext context, dynamic page) {
  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(builder: (context) => page),
    (Route<dynamic> route) => false,
  );
}
